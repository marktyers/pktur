//
//  GalleryController.swift
//  Piktur
//
//  Created by Mark Tyers on 05/11/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit
import AVFoundation

let reuseIdentifier = "Cell"

class GalleryController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var photos = [UIImage]()
    
    @IBAction func getPhoto(sender: UIBarButtonItem) {
        println("getPhoto")
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photos", style: UIAlertActionStyle.Default, handler: {
            (value: UIAlertAction!) in
            println("photos")
            self.getFromPhotos()
        }))
        
        let devices = AVCaptureDevice.devices()
        var camera = false
        for device in devices {
            if (device.hasMediaType(AVMediaTypeVideo)) {
                println("camera detected")
                camera = true
            }
        }
        if camera == true {
            actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: {
                (value: UIAlertAction!) in
                println("camera")
                self.getFromCamera()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func getFromPhotos() {
        println("getFromPhotos")
        let picker = UIImagePickerController()
        picker.delegate = self
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    func getFromCamera() {
        println("getFromCamera")
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: NSDictionary!) {
        self.dismissViewControllerAnimated(true, completion: nil);
        println(info);
        let newImage = info[UIImagePickerControllerOriginalImage] as UIImage
        self.photos.append(newImage)
        println("Array contains \(self.photos.count) images")
        println(self.photos)
        self.collectionView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("segue triggered")
        if let destination = segue.destinationViewController as? EditController {
            println("EditController")
            if let items = self.collectionView.indexPathsForSelectedItems() {
                println("items")
                println(items)
                let item = items[0] as? NSIndexPath
                if let row = item?.row {
                    println("row: \(row)")
                    destination.image = self.photos[row]
                    destination.index = row
                }
            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    override func collectionView(collectionView: UICollectionView,
               cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PhotoCell", forIndexPath: indexPath) as PhotoCell
        cell.image.image = self.photos[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(100, 100)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
