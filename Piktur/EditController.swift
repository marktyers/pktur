//
//  EditController.swift
//  Piktur
//
//  Created by Mark Tyers on 05/11/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class EditController: UIViewController {
    
    var image:UIImage?
    var index:Int?
    
    @IBOutlet weak var photo: UIImageView!
    
    @IBAction func applyFilter(sender: UIBarButtonItem) {
        println("applyFilter")
        let beginImage = CIImage(image: self.image)
        let filter = CIFilter(name: "CISepiaTone")
        filter.setValue(beginImage, forKey: kCIInputImageKey)
        filter.setValue(0.8, forKey: kCIInputIntensityKey)
        self.image = UIImage(CIImage: filter.outputImage)
        self.photo.image = self.image
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.photo.image = self.image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
